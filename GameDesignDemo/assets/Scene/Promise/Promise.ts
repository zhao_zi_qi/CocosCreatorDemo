// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    icon: cc.Node = null;

    start() {
        this.loadRes('img/nan02', cc.SpriteFrame).then((spriteFrame: cc.SpriteFrame) => {
            this.icon.getComponent(cc.Sprite).spriteFrame = spriteFrame;
        })

        this.loadRes('sound/btn_click', cc.AudioClip).then((clip: cc.AudioClip) => {
            cc.audioEngine.playEffect(clip, true);
        })

        this.loadRes('prefab/HelloWorld', cc.Prefab).then((prefab: cc.Prefab) => {
            let node = cc.instantiate(prefab);
            node.parent = this.node;
            node.y = -100;
        })
    }

    loadRes(resPath: string, type: typeof cc.Asset) {
        return new Promise((resolve, reject) => {
            cc.resources.load(resPath, type, (err: Error, asset: cc.Asset) => {
                if (!err) {
                    resolve(asset)
                } else {
                    reject(err);
                }
            })
        });
    }


    // update (dt) {}
}
