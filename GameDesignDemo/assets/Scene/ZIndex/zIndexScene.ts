// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {


    // LIFE-CYCLE CALLBACKS:

    onLoad() {

    }

    start() {
        for (let i = 0; i < 4; i++) {
            let item = this.node.getChildByName("singleColor" + (i + 1));
            if (item) {
                console.log("item.zIndex:", item.zIndex);
                console.log("item.getSiblingIndex():", item.getSiblingIndex());
            }
        }
    }

    // update (dt) {}
}
