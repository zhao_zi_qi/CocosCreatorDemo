// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class Tween extends cc.Component {

    @property(cc.Node)
    ndIcon: cc.Node = null;

    start() {
        this.foreverRotate();
    }

    foreverOpacityChange() {
        cc.tween(this.ndIcon).repeatForever(
            cc.tween().to(
                1, { opacity: 50 }).to(1, { opacity: 255 })
        ).start();
    }

    foreverRotate() {
        cc.tween(this.ndIcon).by(1, { angle: -360 }).repeatForever().start();
    }

    stopTween() {
        cc.Tween.stopAllByTarget(this.ndIcon);
    }

    // update (dt) {}
}
