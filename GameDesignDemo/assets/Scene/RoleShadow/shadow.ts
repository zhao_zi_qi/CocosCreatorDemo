// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class shadow extends cc.Component {

    @property(cc.Sprite)
    shadow: cc.Sprite = null;

    @property(cc.Camera)
    cam: cc.Camera = null

    @property(cc.Node)
    role: cc.Node = null

    start() {
        const rt = new cc.RenderTexture()
        rt.initWithSize(this.shadow.node.width, this.shadow.node.height)
        this.cam.targetTexture = rt
        const sp = new cc.SpriteFrame(rt)
        this.shadow.spriteFrame = sp
        this.shadow.node.on(cc.Node.EventType.SIZE_CHANGED, this.SIZE_CHANGED, this)


        cc.tween(this.role)
            .by(1, { y: 100 })
            .by(1, { y: -100 })
            .union()
            .repeatForever()
            .start()

    }

    SIZE_CHANGED() {
        const rt = this.cam.targetTexture
        rt.initWithSize(this.shadow.node.width, this.shadow.node.height)
        this.cam.targetTexture = rt
        const sp = new cc.SpriteFrame(rt)
        this.shadow.spriteFrame = sp
    }
}
